import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

public class ReadThread implements Runnable{
	
	protected BlockingQueue<String> blockingQueue = null;
	private String thread_name;
	private String file_name;

   public ReadThread(String thread_name, BlockingQueue<String> bl_queue, String fn) {
	  this.thread_name = thread_name;
      blockingQueue = bl_queue;
      file_name = fn;
   }
   
   public void run() {
	   
		try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
		    int ctr = 1;
	    	
	    	String buffer = null;
	    	
		    while ((buffer = br.readLine()) != null) {
		    	if(ctr % 10 == 1) 
		    	{
		    		blockingQueue.put(buffer);
		    	}
		    	else if(ctr % 10 == 3){
			    		ctr = 0;
					} 
		    	ctr++;
				
		    }
		    blockingQueue.put("EOF"); 

		}
		catch(FileNotFoundException e)
		{
			System.out.println("File not found.");
			return;
		}
		catch(IOException e)
		{
			System.out.println("File format error.");
			return;
		}
		catch (InterruptedException e) {
			System.out.println("Reader thread interrupted.");
		}
		
      System.out.println("Reader Thread " +  thread_name + " exiting.");
   }

}
