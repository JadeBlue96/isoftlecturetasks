import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Main {
	
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		BlockingQueue<String> queue = new ArrayBlockingQueue<String>(1024);
		String file = "src/data.txt";
	    ReadThread reader = new ReadThread("read_thread", queue, file);
	    WriteThread writer = new WriteThread(queue, "write_thread", file);
	    
	    new Thread(reader).start();
	    new Thread(writer).start();
		

	}
}
