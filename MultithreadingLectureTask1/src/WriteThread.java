
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.concurrent.BlockingQueue;

public class WriteThread implements Runnable {
	
	protected BlockingQueue<String> blockingQueue = null;
	private String threadName;
	private String fn;

   public WriteThread(BlockingQueue<String> blockingQueue, String name, String fn) {
	  this.blockingQueue = blockingQueue;
      threadName = name;
      this.fn = fn;
   }
   
   public void run() {
	   
	   try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(
		              new FileOutputStream(fn, true), "utf-8"));
			
			
			writer.write("\nNames: ");
			while(true)
			{
				//Thread.sleep(500);
				String buffer = blockingQueue.take();
				if(buffer.equals("EOF")) break;
				writer.write(buffer);
				writer.write("|");
			}
			System.out.println("Writing complete.\n");
			writer.close();
		}
		catch (IOException e) {
			System.out.println("Stream format error.");
	    	return;
		}
	   catch (InterruptedException e) {
			System.out.println("Writer thread interrupted.");
		}
      System.out.println("Writer Thread " +  threadName + " exiting.");
   }
}
