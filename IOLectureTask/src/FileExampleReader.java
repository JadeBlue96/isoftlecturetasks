import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FileExampleReader {
	
	public static List<UserData> file_to_list(String file_name, Integer user_count) throws FileNotFoundException, IOException
	{
		List<UserData> data_list = new ArrayList<UserData>();
		try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
		    String line;
		    int ctr = 1;
		    String cur_name = "";
	    	Integer cur_age = 0;
	    	LocalDate cur_date = LocalDate.now();
	    	
		    while ((line = br.readLine()) != null) {
		    	if(ctr % 10 == 1) cur_name = line;
		    	else if(ctr % 10 == 2) cur_age = Integer.parseInt(line);
		    	else if(ctr % 10 == 3){
						UserData user_data = new UserData(cur_name, cur_age, cur_date);
			    		data_list.add(user_data);
			    		ctr = 0;
					} 
		    	ctr++;
		    }
		   
		    
		    return data_list;
		}
		catch(FileNotFoundException e)
		{
			System.out.println("File not found.");
			return null;
		}
		catch(IOException e)
		{
			System.out.println("File format error.");
			return null;
		}
	}
	
	public static void write_conc_list(String out_file, List<UserData> user_list)
	{
		
			try {
				Writer writer = new BufferedWriter(new OutputStreamWriter(
			              new FileOutputStream(out_file), "utf-8"));
				writer.write("Names: ");
				for(UserData user: user_list)
				{
					writer.write(user.getName() + "|");
				}
				writer.write("\nAges: ");
				for(UserData user: user_list)
				{
					writer.write(user.getAge() + "|");
				}
				writer.write("\nDates: ");
				for(UserData user: user_list)
				{
					writer.write(user.getBirth_date() + "|");
				}
				System.out.println("Writing complete.\n");
				writer.close();
			}
			catch (IOException e) {
				System.out.println("Stream format error.");
		    	return;
			}	
		
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		String inp_file = "src/data.txt";
		List<UserData> user_list = new ArrayList<UserData>();
		user_list = file_to_list(inp_file, 3);
		if(user_list != null)
		{
			for(UserData user: user_list)
			{
				System.out.println(user.toString());
			}
			write_conc_list("src/data_out.txt", user_list);
		}

	}

}
