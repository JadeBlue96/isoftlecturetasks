import java.time.LocalDate;
import java.util.Date;

public class UserData {
	String name;
	Integer age;
	LocalDate birth_date;
	
	UserData() {
		name = "";
		age = 0;
		birth_date = null;
	}
	UserData(String name, Integer age, LocalDate birth_date)
	{
		this.name = name;
		this.age = age;
		this.birth_date = birth_date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public LocalDate getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(LocalDate birth_date) {
		this.birth_date = birth_date;
	}
	@Override
	public String toString() {
		return "UserData [name=" + name + ", age=" + age + ", birth_date=" + birth_date + "]";
	}
	
}
